package com.classFiles;

public class Request {
	private int id;
	private String name, email, phone, message;
	private Boolean isActive;
	
	public Request() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Request(String name, String email, String phone, String message) {
		super();
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.message = message;
	}

	public Request(int id, String name, String email, String phone, String message) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.message = message;
	}
	
	public Request(String name, String email, String phone, String message, Boolean isActive) {
		super();
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.message = message;
		this.isActive = isActive;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	@Override
	public String toString() {
		return "Request [id=" + id + ", name=" + name + ", email=" + email + ", phone=" + phone + ", message=" + message
				+ "]";
	}
	
}
