package com.servletFiles;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.classFiles.Request;
import com.dao.RequestDao;

@WebServlet("/dashboard")
public class DashboardServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Request> activeList = new ArrayList<>();
		List<Request> archiveList = new ArrayList<>();
		try {
			activeList = RequestDao.getActiveData();
			archiveList = RequestDao.getArchiveData();
			request.setAttribute("activeList", activeList);
			request.setAttribute("archiveList", archiveList);
			request.getRequestDispatcher("dashboard.jsp").forward(request, response);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//RequestDispatcher requestDispatch = request.getRequestDispatcher("dashboard");
		int Id = Integer.parseInt(request.getParameter("id"));
		try {
			int status = RequestDao.changeStatus(Id);
			if(status > 0) {
				request.setAttribute("updated", "Successful!");
				doGet(request, response);
			}
			else {
				request.setAttribute("updated", "Invalid Query Id");
				doGet(request, response);
			}
			//requestDispatch.forward(request, response);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
