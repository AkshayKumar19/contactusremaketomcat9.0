package com.servletFiles;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.classFiles.Request;
import com.dao.RequestDao;

@WebServlet("/contactus")
public class ContactUsServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("contactus.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatch = request.getRequestDispatcher("contactus.jsp");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String message = request.getParameter("message");
		
		String saveStatus ="";
		try {
			int status = RequestDao.saveCustomerQueryData(new Request(name, email, phone, message));
			if(status > 0) {
				saveStatus = "Thanks for your query. We'll contact you shortly!";
			}
			else {
				saveStatus = "Server Error! Please, Try again later.";
			}
			request.setAttribute("statusMessage", saveStatus);
			requestDispatch.forward(request, response);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
