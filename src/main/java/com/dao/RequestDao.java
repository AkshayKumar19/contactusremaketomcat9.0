package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.classFiles.Request;

public class RequestDao {
	private static String URL = "jdbc:postgresql://localhost:5432/guest";
	private static String USER = "guest";
	private static String PASSWORD = "guest123";
	private static boolean STATUS = true;
	static Connection con = null;
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("org.postgresql.Driver");
		con = DriverManager.getConnection(URL, USER, PASSWORD);
		return con;
	}
	
	public static int saveCustomerQueryData(Request data) throws ClassNotFoundException, SQLException {
		int status = 0;
		con = RequestDao.getConnection();
		String saveQuery = "insert into contactus values(?,?,?,?,?)";
		PreparedStatement saveStatement = con.prepareStatement(saveQuery);
		saveStatement.setString(1, data.getName());
		saveStatement.setString(2, data.getEmail());
		saveStatement.setString(3, data.getPhone());
		saveStatement.setString(4, data.getMessage());
		saveStatement.setBoolean(5, true);
		status = saveStatement.executeUpdate();
		return status;
	}
	
	public static List<Request> getActiveData() throws ClassNotFoundException, SQLException {
		List<Request> activeDataList = new ArrayList<>();
		con = RequestDao.getConnection();
		String activeDataQuery = "select * from contactus where isActive = ?";
		PreparedStatement activeStatement = con.prepareStatement(activeDataQuery);
		activeStatement.setBoolean(1, STATUS);
		ResultSet activeData = activeStatement.executeQuery();
		while(activeData.next()) {
			activeDataList.add(new Request(activeData.getInt(6),
					activeData.getString(1),
					activeData.getString(2),
					activeData.getString(3),
					activeData.getString(4)));
		}
		return activeDataList;
	}
	
	public static List<Request> getArchiveData() throws ClassNotFoundException, SQLException {
		List<Request> activeDataList = new ArrayList<>();
		con = RequestDao.getConnection();
		String activeDataQuery = "select * from contactus where isActive = ?";
		PreparedStatement activeStatement = con.prepareStatement(activeDataQuery);
		activeStatement.setBoolean(1, !STATUS);
		ResultSet activeData = activeStatement.executeQuery();
		while(activeData.next()) {
			activeDataList.add(new Request(activeData.getInt(6),
					activeData.getString(1),
					activeData.getString(2),
					activeData.getString(3),
					activeData.getString(4)));
		}
		return activeDataList;
	}
	
	public static int changeStatus(int id) throws ClassNotFoundException, SQLException {
		int status = 0;
		boolean idStatus = true;
		con = RequestDao.getConnection();
		String fetchIdDataQuery = "select isActive from contactus where queryId = ?";
		PreparedStatement fetchStatement = con.prepareStatement(fetchIdDataQuery);
		fetchStatement.setInt(1, id);
		ResultSet fetchIdData = fetchStatement.executeQuery();
		while(fetchIdData.next()) {
			idStatus = fetchIdData.getBoolean("isActive");
		}
		
		String changeStatusQuery = "update contactus set isActive = ? where queryId = ?";
		PreparedStatement changeStatusStatement = con.prepareStatement(changeStatusQuery);
		idStatus = !idStatus;
		changeStatusStatement.setBoolean(1, idStatus);
		changeStatusStatement.setInt(2, id);
		status = changeStatusStatement.executeUpdate();
		return status;
	}
}
