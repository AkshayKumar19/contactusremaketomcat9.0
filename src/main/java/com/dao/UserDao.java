package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
	
	public static boolean check(String user, String password) throws ClassNotFoundException, SQLException {
		Connection con =RequestDao.getConnection();
		String checkQuery = "select * from login where uname = ? and pass = ?";
		PreparedStatement checkStatement = con.prepareStatement(checkQuery);
		checkStatement.setString(1, user);
		checkStatement.setString(2, password);
		ResultSet checkResult = checkStatement.executeQuery();
		if(checkResult.next()) {
			return true;
		}
		return false;
	}
}
