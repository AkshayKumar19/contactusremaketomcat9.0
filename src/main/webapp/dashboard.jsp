<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.classFiles.Request"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
Welcome, ${user}.<br>
<h1>Active Data</h1>
<%
    ArrayList<Request> dataLive = (ArrayList<Request>)request.getAttribute("activeList");
    for(Request active : dataLive){%>
    <%=active.getId()%>
    <%=active.getName()%>
    <%=active.getEmail()%>
    <%=active.getPhone()%>
    <%=active.getMessage()%><br>
<%}%>
<h1>Archived Data</h1>
<%
    ArrayList<Request> dataDead = (ArrayList<Request>)request.getAttribute("archiveList");
    for(Request nonActive : dataDead){%>
    <%=nonActive.getId()%>
    <%=nonActive.getName()%>
    <%=nonActive.getEmail()%>
    <%=nonActive.getPhone()%>
    <%=nonActive.getMessage()%><br>
<%}%>
<form action="dashboard" method = "post">
    <input type = "text" name = "id"><br>
    <input type = "submit" value = "UpdateStatus"><br>
</form>
${updated}
</body>
</html>